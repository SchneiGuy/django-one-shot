from django.urls import path
from todos.views import (
    todo_list_list,
    todo_list_detail,
    create_list,
    update_list,
    delete_list,
    create_item,
    update_item,
)

urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", create_list, name="todo_list_create"),
    path("<int:id>/edit/", update_list, name="todo_list_update"),
    path("<int:id>/delete/", delete_list, name="todo_list_delete"),
    path("items/create/", create_item, name="todo_item_create"),
    path("items/<int:id>/edit/", update_item, name="todo_item_update"),
]
